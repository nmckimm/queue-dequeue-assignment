
public class ArrayQueue implements Queue {
    private Object[] Q;
    private int front, rear;
    private int N;

    public ArrayQueue()
    {
        this(1000);
    }

    public ArrayQueue(int size)
    {
        this.N = size;
        Q = new Object[N];
        front = rear = 0;
    }

    @Override
    public void enqueue(Object o)
    {
        if(size() == N-1) { throw new FullQueueException("Queue is full"); }
        Q[rear] = 0;
        rear = (rear+1) % N;
    }

    @Override
    public Object dequeue()
    {
        Object e = Q[front];
        if (isEmpty()) { throw new EmptyQueueException("Queue is empty"); }
        Q[front] = null;
        front = (front+1) % N;
        return e;
    }

    @Override
    public int size()
    {
        return (N + rear - front) % N;
    }

    @Override
    public boolean isEmpty()
    {
        return front==rear;
    }

    @Override
    public Object front()
    {
        if(isEmpty()) { throw new EmptyQueueException("Queue is empty"); }
        return Q[front];
    }

    @Override
    public String toString()
    {
        StringBuffer str = new StringBuffer();
        str.append(size()).append(" / ").append(N).append(" :");
        int i = front;
        while( i!=rear )
        {
            str.append(" ").append(Q[i]);
            i = (i+1) % N;
        }
        return str.toString();
    }
}
