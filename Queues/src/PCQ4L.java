
public class PCQ4L {
    public static void main(String[] args)
    {
        LinkedDeque<String> o = new LinkedDeque<String>();

        o.addFirst(new String("Ireland"));
        System.out.println(o);
        o.removeLast();
        System.out.println(o);
        o.addLast(new String("England"));
        System.out.println(o);
        o.removeFirst();
        System.out.println(o);
        o.addLast(new String("Wales"));
        System.out.println(o);
        o.addFirst(new String("Scotland"));
        System.out.println(o);
        o.addLast(new String("France"));
        System.out.println(o);
        o.removeFirst();
        System.out.println(o);
        o.removeLast();
        System.out.println(o);
        o.addLast(new String("Germany"));
        System.out.println(o);
    }

}
