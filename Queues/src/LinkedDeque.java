public class LinkedDeque<E> implements Deque<E>
{
    private DLNode<E> front;
    private DLNode<E> rear;
    protected int size = 0;

    public LinkedDeque()
    {
        this.front = null;
        this.rear = null;
    }

    @Override
    public int size() { return size; }

    @Override
    public boolean isEmpty() { return (front == null) && (rear == null); }

    @Override
    public E getFirst() throws EmptyDequeException
    {
        if(isEmpty()) { throw new EmptyDequeException("Empty deque"); }
        return front.getElement();
    }

    @Override
    public E getLast() throws EmptyDequeException
    {
        if(isEmpty()) { throw new EmptyDequeException("Empty deque"); }
        return rear.getElement();
    }

    @Override
    public void addFirst (E element)
    {
        DLNode dlnode = new DLNode(element, null, front);
        if(isEmpty()) { rear = dlnode; }
        else { front.setPrev(dlnode); }

        front = dlnode;
        size++;
    }

    @Override
    public void addLast (E element)
    {
        DLNode dlnode = new DLNode(element, rear, null);
        if(isEmpty()) { front = dlnode; }
        else { rear.setNext(dlnode); }

        rear = dlnode;
        size++;
    }

    @Override
    public E removeFirst() throws EmptyDequeException
    {
        E start;
        if(isEmpty()) { throw new EmptyDequeException("Empty deque"); }
        else
        {
            start = front.getElement();
            front = front.getNext();

            if(front == null) { rear = null; }
            else { front.setPrev(null); }
        }
        size--;
        return start;
    }

    @Override
    public E removeLast() throws EmptyDequeException
    {
        E end;
        if(isEmpty()) { throw new EmptyDequeException("Empty deque"); }
        else
        {
            end = rear.getElement();
            rear = rear.getPrev();

            if(rear == null) { front = null; }
            else { rear.setNext(null); }
        }
        size--;
        return end;
    }

    @Override
    public String toString()
    {
        StringBuilder str = new StringBuilder();
        DLNode dlnode = this.front;
        if(dlnode == null) { str.append("Deque is empty"); }
        else { str.append("Deque contents : "); }
        while(dlnode != null)
        {
            str.append(dlnode.getElement() + "\t");
            dlnode = dlnode.getNext();
        }
        return str.toString();
    }
}
